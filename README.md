#**Note**
**Recursive Backtrack Maze Generator**
This project is never meant to be super Optimize or anything like that. 
I just want to try if I can Replicate https://en.wikipedia.org/wiki/Maze_generation_algorithm Algorithm into UE4 and tried to make a maze system out of it.

If you are trying to use this project for your game I will highly recommend not due to a lot of hard hacks are implemented during the Development of this Project. 
I have never plan to maintain this project and it's a 1 Day Project. But there are few stuff you can do to make it run more efficiently.
Again I still recommend just use this as a Reference to start your own Maze Generator.
It's just a Concept. You can make any kind of shape with this Algorithm.

##The Concept Behind this Algorithm
U have a Cell. The cell have Neighbours. The Cell doesn't needs to know what kind of Shape this maze will end up. The cells Just needs to know who it's
Neighbour is. 

Then u have a Manager or some sort that will Spawn all these cells. After Spawning the entire Shape of Maze, 1 Manager will try to **Visit** 
all the cells starting from the first cell. 

When it visit the next cell, It will **remove the walls in between cells**. It will **keep track** of all the Cells that 
it is Visiting.(Stack). After it run out of Option to Visit the next Cell. It will **Backtrack** To Previous visited cells until it find a New Neighbour.

This Process will go on until the Entire Cell is visited. Thus, A Maze is formed.

##Tips You can take advantage of from this Algorithm.

1. U can keep track of the Dead ends each time it backtrack.
2. U can keep track of how long it takes to reach 1 point and setup some algorithm for rewards/Punishment System. 
3. Different Shape can be Implement For this Algorithm. (I use Rectangle because it's easier to test it out)
4. U can Make Custom A star With this. This algorithm and A star It's like bread and butter.

##Here are my suggestion for further Optimization. 	

1. Do not spawn Pure Static Mesh. You can always Try HISM or ISM.
2. Redundant wall Spawns. U can reduce the Frame drop by spawning on 1 Walls in between. Current Algorithm Spawns 2 walls and delete them afterwords.
3. Data Structure Improvement. TArray is not a very efficient when it comes to Memory Management. You can always change the Stacks Data Structure. 
4. The Data structure to Keep Wall is also not very efficient. Keep them as Map would be much better.
5. As Mention Above. I Have 1 Manager that will Spawn the Shape before removing the walls. You can actually have 1 PreSpawned Shape then only remove the walls.
