// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SimpleMazeGridActor.generated.h"

UENUM(BlueprintType)
enum class EDirection : uint8
{
	North,
	South,
	East,
	West,
};

UENUM(BlueprintType)
enum class EInverseRandDirection : uint8
{
	South,
	North,
	West,
	East,
};


UCLASS()
class ALLGENERATORSRND_API ASimpleMazeGridActor : public AActor
{
	GENERATED_UCLASS_BODY()

public:
	UFUNCTION()
	UStaticMeshComponent * GetSMFloor() { return SM_Floor; }

	UFUNCTION(BlueprintGetter)
	bool GetIsVisited() const { return bIsVisited; }

	UFUNCTION(BlueprintSetter)
	void SetIsVisited(const bool bShouldVisit) { bIsVisited = bShouldVisit; }

	UStaticMeshComponent* GetWallFromDirection(EDirection Direction);

	EDirection ConverInverseDirection(EInverseRandDirection InverseDir) const;

	void SetupNeighbour(ASimpleMazeGridActor * NewNeighbour, EDirection AtDir);

	TMap<EDirection, ASimpleMazeGridActor *> GetNeighbours() { return Neighbours; };

protected:
	virtual void OnConstruction(const FTransform & Transform)override;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite , BlueprintSetter = SetIsVisited, BlueprintGetter = GetIsVisited, Category = "Grid")
	uint8 bIsVisited : 1;

private:
	UPROPERTY(VisibleInstanceOnly, Category = "Grids")
	TMap<EDirection, ASimpleMazeGridActor *> Neighbours;

	UPROPERTY(EditDefaultsOnly, Category = "Maze Components")
	UStaticMesh* SM_WallSelection;

	UPROPERTY(VisibleDefaultsOnly, Category = "Maze Components")
	USceneComponent * Root;

	/*
	 *West 0
	 *East 1 
	 *South 2
	 *North 3
	 */
	UPROPERTY(VisibleDefaultsOnly, Category = "Maze Components")
	TArray<UStaticMeshComponent *> SM_Walls;

	UPROPERTY(VisibleDefaultsOnly, Category = "Maze Components")
	UStaticMeshComponent * SM_Floor;

	UPROPERTY(VisibleDefaultsOnly, Category = "Maze Components")
	UStaticMeshComponent * SM_Ceiling;
};
