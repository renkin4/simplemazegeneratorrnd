// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Managers/RNDInfo.h"
#include "SimpleMazeGridActor.h"
#include "SimpleGridMazeGenerator.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum class ERandDirection : uint8
{
	North,
	South,
	East,
	West,
};

USTRUCT()
struct FMazeGridRow
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY()
	TArray<ASimpleMazeGridActor*> Columns;

	//default properties
	FMazeGridRow() {}

	FMazeGridRow(int32 SizeofColumns) 
	{
		Columns.Reserve(SizeofColumns);
	}
};

USTRUCT(BlueprintType)
struct FMazeGrid
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, Category = "Grids")
	int32 MaxCol;

	UPROPERTY(EditAnywhere, Category = "Grids")
	int32 MaxRow;

	UPROPERTY()
	TArray<FMazeGridRow> Rows;

	void AddNewRow() { Rows.Add(FMazeGridRow(MaxCol)); }

	// Quite Heavy Iterated use it wisely
	void Clear()
	{
		if (Rows.Num() <= 0) return;
		//~~~~~~~~~~~~~~~

		//Destroy any walls
		const int32 RowTotal = Rows.Num();
		const int32 ColTotal = Rows[0].Columns.Num();

		for (int32 x = 0; x < RowTotal; x++)
		{
			for (int32 y = 0; y < ColTotal; y++)
			{
				if (Rows[x].Columns[y] && Rows[x].Columns[y]->IsValidLowLevel())
				{
					// Can Insert a random Life span value for a Better Visual
					Rows[x].Columns[y]->Destroy();
				}
			}
		}

		//Empty
		for (int32 x = 0; x < Rows.Num(); x++)
		{
			Rows[x].Columns.Empty();
		}
		Rows.Empty();
	}
	//default properties
	FMazeGrid() {}

	FMazeGrid(int32 SizeOfRow) 
	{
		Rows.Reserve(SizeOfRow);
	}
};

UCLASS(Blueprintable, BlueprintType)
class ALLGENERATORSRND_API ASimpleGridMazeGenerator : public ARNDInfo
{
	GENERATED_UCLASS_BODY()
	
protected:
	virtual void BeginPlay() override;

	virtual void PostInitializeComponents() override;

	virtual void SpawningGrid();

	virtual void InitGenerator();

	virtual void StartVisitGrid();

	virtual void TryVisitNextGrid();

	virtual void NotifyGridNeighbour(ASimpleMazeGridActor* NewGrid);

private:
	/* Generator Properties */
	UPROPERTY(Transient, VisibleInstanceOnly)
	int32 SizeOfGridInX;
	
	UPROPERTY(EditDefaultsOnly, Category = "Grids")
	TSubclassOf<ASimpleMazeGridActor> GridToSpawn;

	UPROPERTY(Transient, VisibleInstanceOnly)
	uint8 CurrentRow;

	UPROPERTY(Transient, VisibleInstanceOnly)
	uint8 CurrentCol;

	UPROPERTY(EditAnywhere, Category = "Grids")
	FMazeGrid MazeGrid;

	UPROPERTY(Transient, VisibleInstanceOnly)
	FVector2D PreviousVisitVec;

	UPROPERTY(Transient, VisibleInstanceOnly)
	FVector2D CurrentVisitVec;

	UPROPERTY(Transient, VisibleInstanceOnly)
	FVector2D NextVisitVec;

	UPROPERTY(Transient, VisibleInstanceOnly)
	int32 NumberOfGridsVisited;

	UPROPERTY(Transient, VisibleInstanceOnly)
	TArray<FVector2D> StackOfVisitedGrids;
	/* End of Generator Properties */

	FTimerHandle SpawningGridHandler;

	FTimerHandle StartVisitGridHandler;

	FVector2D ConvertRandDirToFVector2D(ERandDirection Dir) const;

	void ResetGenerator();

	ASimpleMazeGridActor* SelectGridFrom2DArray(int32 RowVal, int32 ColVal);

	UPROPERTY(EditDefaultsOnly, Category = "Grids")
	UMaterialInterface* SelectedGridMat;

	UPROPERTY(EditDefaultsOnly, Category = "Grids")
	UMaterialInterface* VisitedGridMat;

	UPROPERTY(EditDefaultsOnly, Category = "Grids")
	UMaterialInterface* DeletedWallMat;
};
