// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "RNDFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class ALLGENERATORSRND_API URNDFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintPure, Category = "RNDFunctionLibrary")
	static FVector GetStaticMeshCompSize(class UStaticMeshComponent* MeshComp);

	UFUNCTION(BlueprintPure, Category = "RNDFunctionLibrary")
	static FVector GetStaticMeshCompSizeInX(class UStaticMeshComponent* MeshComp);

	UFUNCTION(BlueprintPure, Category = "RNDFunctionLibrary")
	static FVector GetStaticMeshCompSizeInY(class UStaticMeshComponent* MeshComp);

	UFUNCTION(BlueprintPure, Category = "RNDFunctionLibrary")
	static FVector GetStaticMeshCompSizeInZ(class UStaticMeshComponent* MeshComp);

	template <class T>
	static T* GetDefaultObjectCast(UClass * ObjectClass) 
	{
		return CastChecked<T>(ObjectClass->GetDefaultObject());
	}
	
};
