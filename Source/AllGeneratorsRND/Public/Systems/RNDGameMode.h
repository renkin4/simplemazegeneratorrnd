// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "RNDGameMode.generated.h"

/**
 * 
 */

class ARNDGameState;
class ASimpleGridMazeGenerator;

UCLASS()
class ALLGENERATORSRND_API ARNDGameMode : public AGameMode
{
	GENERATED_UCLASS_BODY()
	
protected:
	virtual void PreInitializeComponents() override;
	
private:
	UPROPERTY(Transient)
	ARNDGameState * RNDGameState;

	UPROPERTY(Transient)
	ASimpleGridMazeGenerator* SimpleGridMazeGenerator;

	UPROPERTY(EditDefaultsOnly, Category = "Grids")
	TSubclassOf<ASimpleGridMazeGenerator> SimpleGridMazeGeneratorClass;
};
