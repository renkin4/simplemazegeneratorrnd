// Fill out your copyright notice in the Description page of Project Settings.

#include "RNDFunctionLibrary.h"
#include "Components/StaticMeshComponent.h"

FVector URNDFunctionLibrary::GetStaticMeshCompSize(class UStaticMeshComponent* MeshComp)
{
	if (!MeshComp)
		return FVector::ZeroVector;

	FVector MinBound, MaxBound;
	MeshComp->GetLocalBounds(MinBound, MaxBound);

	return (MinBound - MaxBound) * MeshComp->GetRelativeTransform().GetScale3D();
}

FVector URNDFunctionLibrary::GetStaticMeshCompSizeInX(UStaticMeshComponent * MeshComp)
{
	if (!MeshComp)
		return FVector::ZeroVector;

	FVector MinBound, MaxBound;
	MeshComp->GetLocalBounds(MinBound, MaxBound);

	FVector ReturnVector = FVector(MinBound.X - MaxBound.X, 0.f, 0.f) * MeshComp->GetRelativeTransform().GetScale3D();

	return ReturnVector;
}

FVector URNDFunctionLibrary::GetStaticMeshCompSizeInY(UStaticMeshComponent * MeshComp)
{
	if (!MeshComp)
		return FVector::ZeroVector;

	FVector MinBound, MaxBound;
	MeshComp->GetLocalBounds(MinBound, MaxBound);

	FVector ReturnVector = FVector(0.f, MinBound.Y - MaxBound.Y, 0.f) * MeshComp->GetRelativeTransform().GetScale3D();

	return ReturnVector;
}

FVector URNDFunctionLibrary::GetStaticMeshCompSizeInZ(UStaticMeshComponent * MeshComp)
{
	if (!MeshComp)
		return FVector::ZeroVector;

	FVector MinBound, MaxBound;
	MeshComp->GetLocalBounds(MinBound, MaxBound);

	FVector ReturnVector = FVector( 0.f, 0.f, MinBound.Z - MaxBound.Z) * MeshComp->GetRelativeTransform().GetScale3D();

	return ReturnVector;
}

