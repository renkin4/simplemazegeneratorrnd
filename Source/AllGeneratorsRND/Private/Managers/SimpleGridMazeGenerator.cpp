// Fill out your copyright notice in the Description page of Project Settings.

#include "SimpleGridMazeGenerator.h"
#include "RNDFunctionLibrary.h"
#include "Components/StaticMeshComponent.h"

#define SpawningGridDelay .01f
#define VisitGridDelay .02f

ASimpleGridMazeGenerator::ASimpleGridMazeGenerator(const class FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{
	ResetGenerator();

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void ASimpleGridMazeGenerator::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void ASimpleGridMazeGenerator::BeginPlay()
{
	Super::BeginPlay();
	InitGenerator();
	ResetGenerator();

	MazeGrid.AddNewRow();

	GetWorldTimerManager().SetTimer(SpawningGridHandler, this, &ASimpleGridMazeGenerator::SpawningGrid, SpawningGridDelay, true);
}

void ASimpleGridMazeGenerator::InitGenerator()
{
	ensure(GridToSpawn->GetDefaultObject());
	const FVector TempVector = URNDFunctionLibrary::GetStaticMeshCompSizeInX(URNDFunctionLibrary::GetDefaultObjectCast<ASimpleMazeGridActor>(GridToSpawn)->GetSMFloor());
	// For some Reason i can't get A Simple Maze Grid Actor with Normal GetDefaultObject
	SizeOfGridInX = FMath::CeilToInt(TempVector.Size());

	UE_LOG(LogTemp, Warning, TEXT("%d Ceiled Value , %f Normal Value"), SizeOfGridInX, TempVector.Size());
	UE_LOG(LogTemp, Warning, TEXT("%s Vector"), *TempVector.ToCompactString());

	StackOfVisitedGrids.Reserve(MazeGrid.MaxCol * MazeGrid.MaxRow);
}

void ASimpleGridMazeGenerator::StartVisitGrid()
{
	CurrentVisitVec = FVector2D(0.f, 0.f);
	ASimpleMazeGridActor * CurrentVisitGrid = SelectGridFrom2DArray((int32)CurrentVisitVec.X, (int32)CurrentVisitVec.Y);
	CurrentVisitGrid->GetSMFloor()->SetMaterial(0, SelectedGridMat);
	CurrentVisitGrid->SetIsVisited(true);

	GetWorldTimerManager().SetTimer(StartVisitGridHandler, this, &ASimpleGridMazeGenerator::TryVisitNextGrid, VisitGridDelay, true);
}

void ASimpleGridMazeGenerator::TryVisitNextGrid()
{
	if (NumberOfGridsVisited >= MazeGrid.MaxRow*MazeGrid.MaxCol) 
	{
		GetWorldTimerManager().ClearTimer(StartVisitGridHandler);
		CurrentVisitVec = FVector2D(0.f, 0.f);
		return;
	}

	ensure(VisitedGridMat && DeletedWallMat && SelectedGridMat);

	ASimpleMazeGridActor * CurrentVisitGrid = SelectGridFrom2DArray((int32)CurrentVisitVec.X, (int32)CurrentVisitVec.Y);
	ASimpleMazeGridActor * PreviousVisitedGrid = nullptr;

	uint8 RandDir = (uint8)(FMath::RandRange(0, 3)) % CurrentVisitGrid->GetNeighbours().Num();
	uint8 InterateIndex = 0;
	uint8 OutDir = 0;

	// Randomly select A Direction From Current Grid
	for (auto & Dir : CurrentVisitGrid->GetNeighbours())
	{
		if (RandDir == InterateIndex) 
		{
			OutDir = (uint8)Dir.Key;
			break;
		}

		OutDir = (uint8)Dir.Key;
		++InterateIndex;
	}
	
	NextVisitVec = CurrentVisitVec + ConvertRandDirToFVector2D((ERandDirection)OutDir);

	UE_LOG(LogTemp, Warning, TEXT("CurrentVisitVec (%s), NextVisitVec (%s)"), *CurrentVisitVec.ToString(), *NextVisitVec.ToString());

	ASimpleMazeGridActor * NextVisitGrid = SelectGridFrom2DArray((int32)NextVisitVec.X, (int32)NextVisitVec.Y);

	// Check if it's Visited. If True = Stuck and go back stack to look for not visited Neighbour
	if (NextVisitGrid->GetIsVisited()) 
	{
		bool bNonVisitedNeighbour = false;
		FVector2D TempGridLocHolder = CurrentVisitVec;
		//Check for Current Grid before Moving On to Check Stack
		for (auto & Dir : CurrentVisitGrid->GetNeighbours())
		{
			if (!Dir.Value->GetIsVisited()) 
			{
				bNonVisitedNeighbour = true;
				NextVisitVec = TempGridLocHolder + ConvertRandDirToFVector2D((ERandDirection)Dir.Key);
				OutDir = (uint8)Dir.Key;
				break;
			}
		}

		if (!bNonVisitedNeighbour) 
		{
			for (int32 x = StackOfVisitedGrids.Num() - 1; x >= 0; --x)
			{
				PreviousVisitVec = CurrentVisitVec;
				CurrentVisitVec = StackOfVisitedGrids[x];
				// Loop for stack neighbour and check if it's visited
				ASimpleMazeGridActor * StackGrid = SelectGridFrom2DArray((int32)CurrentVisitVec.X, (int32)CurrentVisitVec.Y);

				CurrentVisitGrid->GetSMFloor()->SetMaterial(0, VisitedGridMat);
				CurrentVisitGrid = StackGrid;

				StackOfVisitedGrids.RemoveAt(x);

				for (auto & Dir : StackGrid->GetNeighbours())
				{
					if (!Dir.Value->GetIsVisited())
					{
						bNonVisitedNeighbour = true;
						NextVisitVec = CurrentVisitVec + ConvertRandDirToFVector2D((ERandDirection)Dir.Key);
						OutDir = (uint8)Dir.Key;
						break;
					}
				}

				if (bNonVisitedNeighbour)
					break;
				else
					CurrentVisitGrid->GetSMFloor()->SetMaterial(0, SelectedGridMat);
			}
		}

		// override
		NextVisitGrid = SelectGridFrom2DArray((int32)NextVisitVec.X, (int32)NextVisitVec.Y);

		UE_LOG(LogTemp, Warning, TEXT("Visited :: CurrentVisitVec (%s), NextVisitVec (%s)"), *CurrentVisitVec.ToString(), *NextVisitVec.ToString());
	}
	
	CurrentVisitGrid->GetSMFloor()->SetMaterial(0, VisitedGridMat);
	NextVisitGrid->GetSMFloor()->SetMaterial(0, SelectedGridMat);

	PreviousVisitVec = CurrentVisitVec;
	CurrentVisitVec = NextVisitVec;

	PreviousVisitedGrid = CurrentVisitGrid;
	CurrentVisitGrid = NextVisitGrid;
	NextVisitGrid->SetIsVisited(true);

	if (PreviousVisitedGrid && CurrentVisitGrid)
	{
		UPrimitiveComponent* PreviousWall = PreviousVisitedGrid->GetWallFromDirection((EDirection)OutDir);
		UPrimitiveComponent * CurrentWall = CurrentVisitGrid->GetWallFromDirection(CurrentVisitGrid->ConverInverseDirection((EInverseRandDirection)OutDir));

		if (PreviousWall && CurrentWall)
		{
			PreviousWall->DestroyComponent(false);
			CurrentWall->DestroyComponent(false);
		}
	}

	++NumberOfGridsVisited;
	StackOfVisitedGrids.Add(PreviousVisitVec);
}

void ASimpleGridMazeGenerator::NotifyGridNeighbour(ASimpleMazeGridActor* NewGrid)
{
	// No Point Calculating for the first Spawned Grid
	if (MazeGrid.Rows.Num() <= 0 || MazeGrid.Rows[0].Columns.Num() <= 1)
		return;

	// Skipping north because we spawn towards North
	for (uint8 x = 1; x < 4; ++x) 
	{
		EDirection DirToCheck = (EDirection)x;

		const FVector2D CurrentGridLocToCheck = FVector2D(CurrentRow, CurrentCol) + ConvertRandDirToFVector2D((ERandDirection)DirToCheck);

		// Check if it's out of bound if yes Continue
		if (CurrentGridLocToCheck.X <= -1 || CurrentGridLocToCheck.X >= MazeGrid.MaxRow || CurrentGridLocToCheck.Y <= -1 || CurrentGridLocToCheck.Y >= MazeGrid.MaxCol)
		{
			UE_LOG(LogTemp, Log, TEXT("It's Out Of Bound. There is no Grid at (%s)"), *CurrentGridLocToCheck.ToString());
			continue;// Change this to continue later
		}

		// If grid Exist add it
		if (MazeGrid.Rows.Num() - 1 >= CurrentGridLocToCheck.X && MazeGrid.Rows[CurrentGridLocToCheck.X].Columns.Num() - 1 >= CurrentGridLocToCheck.Y)
		{
			ASimpleMazeGridActor * SelectedGrid = SelectGridFrom2DArray(CurrentGridLocToCheck.X, CurrentGridLocToCheck.Y);
			SelectedGrid->SetupNeighbour(NewGrid, SelectedGrid->ConverInverseDirection((EInverseRandDirection)DirToCheck));

			NewGrid->SetupNeighbour(SelectedGrid, DirToCheck);
		}

	}
	
}

FVector2D ASimpleGridMazeGenerator::ConvertRandDirToFVector2D(ERandDirection Dir) const
{
	FVector2D TempVectorHolder;

	switch (Dir)
	{
	case ERandDirection::North:
		TempVectorHolder = FVector2D(0.f,1.f);
		break;
	case ERandDirection::South:
		TempVectorHolder = FVector2D(0.f, -1.f);
		break;
	case ERandDirection::East:
		TempVectorHolder = FVector2D(1.f, 0.f);
		break;
	case ERandDirection::West:
		TempVectorHolder = FVector2D(-1.f, 0.f);
		break;
	default:
		break;
	}

	return TempVectorHolder;
}

void ASimpleGridMazeGenerator::ResetGenerator()
{
	CurrentCol = 0;
	CurrentRow = 0;
}

void ASimpleGridMazeGenerator::SpawningGrid()
{
	// Finish Spawning Grid now move to Visiting Grid
	if (CurrentRow > MazeGrid.MaxRow - 1)
	{
		GetWorldTimerManager().ClearTimer(SpawningGridHandler);
		ResetGenerator();

		StartVisitGrid();
		return;
	}

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = Instigator;
	FTransform SpawnTransform = FTransform(FVector(CurrentCol * SizeOfGridInX, CurrentRow * SizeOfGridInX, 0.f));

	ASimpleMazeGridActor* TempGrid = GetWorld()->SpawnActor<ASimpleMazeGridActor>(GridToSpawn, SpawnTransform, SpawnInfo);
	MazeGrid.Rows[CurrentRow].Columns.Add(TempGrid);

	NotifyGridNeighbour(TempGrid);

	// Add Row here
	if (CurrentCol >= MazeGrid.MaxCol -1)
	{
		CurrentCol = 0;
		MazeGrid.AddNewRow();

		++CurrentRow;
	}
	// Add Col Here
	else 
	{
		++CurrentCol;
	}
}

ASimpleMazeGridActor* ASimpleGridMazeGenerator::SelectGridFrom2DArray(int32 RowVal, int32 ColVal)
{
	ensure(MazeGrid.Rows.Num() >= RowVal && MazeGrid.Rows[RowVal].Columns[ColVal]);

	return MazeGrid.Rows[RowVal].Columns[ColVal];
}
