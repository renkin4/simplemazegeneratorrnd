// Fill out your copyright notice in the Description page of Project Settings.

#include "RNDGameMode.h"
#include "RNDGameState.h"
#include "SimpleGridMazeGenerator.h"

ARNDGameMode::ARNDGameMode(const class FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{
	GameStateClass = ARNDGameState::StaticClass();

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void ARNDGameMode::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	RNDGameState = Cast<ARNDGameState>(GameState);
	ensure(RNDGameState);

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = Instigator;
	SpawnInfo.ObjectFlags |= RF_Transient;	// We never want to save game states or network managers into a map						
		
	if (SimpleGridMazeGeneratorClass == nullptr)// Fallback to default SimpleGridMazeGeneratorClass if none was specified.
	{
		UE_LOG(LogGameMode, Warning, TEXT("No SimpleGridMazeGeneratorClass was specified in %s (%s)"), *GetName(), *GetClass()->GetName());
		SimpleGridMazeGeneratorClass = ASimpleGridMazeGenerator::StaticClass();
	}

	SimpleGridMazeGenerator = GetWorld()->SpawnActor<ASimpleGridMazeGenerator>(SimpleGridMazeGeneratorClass, SpawnInfo);
	GetWorld()->SetGameState(GameState);

}
