// Fill out your copyright notice in the Description page of Project Settings.

#include "SimpleMazeGridActor.h"
#include "Components/StaticMeshComponent.h"
#include "RNDFunctionLibrary.h"

#define MaxAmountOfWalls 4

// Sets default values
ASimpleMazeGridActor::ASimpleMazeGridActor(const class FObjectInitializer & ObjectInitializer)
	:Super(ObjectInitializer)
{
	Root = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("Root")); 
	RootComponent = Root;

	/*Walls Setups*/
	for (uint8 x = 0; x < MaxAmountOfWalls; ++x)
	{
		FName WallName = FName(*FString::Printf(TEXT("Wall%d"), x));
		UStaticMeshComponent* MeshComponent = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, WallName);
		MeshComponent->SetupAttachment(RootComponent);
		MeshComponent->SetMobility(EComponentMobility::Stationary);

		SM_Walls.Add(MeshComponent);
	}

	/*END OF Wall Setup*/

	/*Default Walls And Ceiling Setups*/

	SM_Floor = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("Floors"));
	SM_Floor->SetupAttachment(RootComponent);
	SM_Floor->SetMobility(EComponentMobility::Stationary);

	SM_Ceiling = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("Ceilings"));
	SM_Ceiling->SetupAttachment(RootComponent);
	SM_Ceiling->SetMobility(EComponentMobility::Stationary);

	/*End of walls and ceiling setups*/

	bIsVisited = false;

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

UStaticMeshComponent* ASimpleMazeGridActor::GetWallFromDirection(EDirection Direction)
{
	UStaticMeshComponent* SM_Holder = nullptr;

	switch (Direction)
	{
	case EDirection::North:
		SM_Holder = SM_Walls[3];
		break;
	case EDirection::South:
		SM_Holder = SM_Walls[2];
		break;
	case EDirection::East:
		SM_Holder = SM_Walls[1];
		break;
	case EDirection::West:
		SM_Holder = SM_Walls[0];
		break;
	default:
		break;
	}

	return SM_Holder;
}

EDirection ASimpleMazeGridActor::ConverInverseDirection(EInverseRandDirection InverseDir) const
{
	EDirection ReturnDir = EDirection::East;

	switch (InverseDir)
	{
	case EInverseRandDirection::North:
		ReturnDir = EDirection::North;
		break;
	case EInverseRandDirection::South:
		ReturnDir = EDirection::South;
		break;
	case EInverseRandDirection::East:
		ReturnDir = EDirection::East;
		break;
	case EInverseRandDirection::West:
		ReturnDir = EDirection::West;
		break;
	default:
		break;
	}

	return ReturnDir;
}

void ASimpleMazeGridActor::SetupNeighbour(ASimpleMazeGridActor * NewNeighbour, EDirection AtDir)
{
	Neighbours.Emplace(AtDir, NewNeighbour);
}

void ASimpleMazeGridActor::OnConstruction(const FTransform & Transform)
{
	const float FloorSizeInX = (URNDFunctionLibrary::GetStaticMeshCompSizeInX(SM_Floor).Size() * .5f);
	const float FloorSizeInY = (URNDFunctionLibrary::GetStaticMeshCompSizeInY(SM_Floor).Size() * .5f);

	FVector SpawnLoc = FVector::ZeroVector;
	bool bShouldInverse = false;

	if (SM_WallSelection)
	{
		for (UStaticMeshComponent * Wall : SM_Walls)
		{
			Wall->SetStaticMesh(SM_WallSelection);
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Please Setup the Static Mesh for (%s) and restart your engine"), *GetClass()->GetName())
	}

	for (uint8 x = 0; x < MaxAmountOfWalls; ++x) 
	{
		const float InverseFloat = bShouldInverse ? 1 : -1;
		const float FloorSizeInZ = (URNDFunctionLibrary::GetStaticMeshCompSizeInZ(SM_Walls[x]).Size() * .5f);
		const float WallScale = 2.6f;
		const float ThinessOfWall = .03f;

		if (x >= FMath::FloorToInt(MaxAmountOfWalls * .5f)) 
		{
			SM_Walls[x]->SetRelativeScale3D(FVector(ThinessOfWall, WallScale * SM_Floor->GetRelativeTransform().GetScale3D().Y, 1.f));
			
			SpawnLoc = FVector((FloorSizeInX * InverseFloat), 0.f, FloorSizeInZ);
		}
		else 
		{
			SM_Walls[x]->SetRelativeScale3D(FVector(WallScale * SM_Floor->GetRelativeTransform().GetScale3D().X, ThinessOfWall, 1.f));

			SpawnLoc = FVector(0.f, FloorSizeInY * InverseFloat, FloorSizeInZ);
		}

		SM_Walls[x]->SetRelativeLocation(SpawnLoc);
		bShouldInverse = !bShouldInverse;
	}
	

}

